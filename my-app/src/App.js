import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './components/pages/home'
import Navigation from './components/shared/navigation'
import Details from './components/pages/care-provider/patientDetails'
// import AdminDetails from './components/pages/super-admin/patientDetailsAdmin'
import Admin from './components/pages/super-admin/providersAdmin'
import Search from './components/pages/care-provider/search'
import SearchAdmin from './components/pages/super-admin/searchAdmin'

function App() {
  return (
    <BrowserRouter>
    <Navigation />
                <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/details" component={Details} />
          {/* <Route exact path="/adminDetails" component={AdminDetails} /> */}
          <Route exact path="/search" component={Search} />
          <Route exact path="/adminSearch" component={SearchAdmin} />
          <Route exact path="/admin" component={Admin} />
        </Switch>
    </BrowserRouter>
  )
}

export default App;
