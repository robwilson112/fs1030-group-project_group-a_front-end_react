import React, { useState } from 'react';
import { Button,  Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import NewProviderForm from '../super-admin/newProviderForm';

const ModalForm = (props) => {
 
    const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

//   
  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>New Care Provider</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Create New Care Provider</ModalHeader>
        
        <ModalBody> 
          
          <NewProviderForm />
        
        </ModalBody>

       
      </Modal>
    </div>
  );
}

export default ModalForm;