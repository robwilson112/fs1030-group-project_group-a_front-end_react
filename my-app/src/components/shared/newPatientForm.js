import React, { useState } from 'react';
import { Button,  Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { useHistory } from "react-router-dom";

const NewPatientForm = () => {
   
    // const [newPatient, setNewPatient] = useState({ 
    //     name: "", 
    //     email: "", 
    //     address: "", 
    //     city: "",  
    //     province: "", 
    //     postal "", 
    //     healthCard: "", 
    //     bloodType: "",
    //     DOB: "",
    //  });


    // const history = useHistory();

    // const handleChange = (event) => {
    //     setNewPatient((prevState) => ({
    //     ...prevState,
    //     [event.target.name]: event.target.value,
    //     }));
    // };

    // const handleSubmit = (event) => {
    //     fetch("/api/", {
    //     method: "post",
    //     headers: {
    //         Accept: "application/json",
    //         "Content-Type": "application/json",
    //     },

    //     //make sure to serialize your JSON body
    //     body: JSON.stringify(newPatient),
    //     }).then((response) => response.json());
    //     history.push("/");
    // };
    
    return(
        

        <Form>
                <FormGroup>
                    <Label for="patientName">Full Name</Label>
                    <Input 
                        type="text"
                        name="patientName" 
                        id="patientName" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input 
                        type="email" 
                        name="email"
                        id="exampleEmail" 
                         // value={newPatient.email} 
                        // onChange={handleChange} 
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientAdd">Address</Label>
                    <Input 
                        type="text" 
                        name="patientAdd"
                        id="patientAdd" 
                        // value={newPatient.address} 
                        // onChange={handleChange} 
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientCity">City</Label>
                    <Input 
                        type="text"
                        name="patientCity"
                        id="patientCity" 
                         // value={newPatient.city} 
                        // onChange={handleChange} 
                      />
                </FormGroup>
                <FormGroup>
                    <Label for="patientProv">Province</Label>
                    <Input 
                        type="text" 
                        name="patientProv" 
                        id="patientProv" 
                         // value={newPatient.province} 
                        // onChange={handleChange} 
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientPC">Postal Code</Label>
                    <Input 
                        type="text" 
                        name="patientPC"
                        id="patientPC" 
                        // value={newPatient.postal} 
                        // onChange={handleChange} 
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientHC">Health Card</Label>
                    <Input 
                        type="text" 
                        name="patientHC" 
                        id="patientHC"
                        // value={newPatient.healthCard} 
                        // onChange={handleChange} 
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientBlood">Blood Type</Label>
                    <Input 
                        type="text" 
                        name="patientBlood" 
                        id="patientBlood"
                         // value={newPatient.bloodType} 
                        // onChange={handleChange} 
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientDOB">Date of Birth</Label>
                    <Input 
                        type="date"
                        name="patientDOB"
                        id="patientDOB" 
                        // value={newPatient.DOB} 
                        // onChange={handleChange} 
                      />
                </FormGroup>
                    <input type="submit" value="Submit" />
               </Form>

  )

}

export default NewPatientForm

            