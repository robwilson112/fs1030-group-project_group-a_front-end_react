import React, { useState } from 'react';
import { Button,  Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import UpdatePatientForm from './updatePatientForm';

const UpdateModalForm = (props) => {
 
    const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

//   
  const toggle = () => setModal(!modal);

  return (
    <div className="updateModal">
      <Button color="danger" onClick={toggle}>Update Patient Details</Button>
      <Modal isOpen={modal} toggle={toggle} contentClassName="custom-modal-style" className={className}>
        <ModalHeader toggle={toggle}>Update PatientDetails</ModalHeader>
        
        <ModalBody> 
          
          <UpdatePatientForm />
        
        </ModalBody>

       
      </Modal>
    </div>
  );
}

export default UpdateModalForm;