import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'


const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)

    return (
        <Navbar dark color="dark" expand="md" fixed="top">
            <Container>
            <NavbarBrand href=""><span className="logo">&#9877;&#65039;</span> EMR Mock Up</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/">Home</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/search">Search Patients</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/details">Patient Details</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/adminSearch">*SA* Search Patients</NavLink>
                    </NavItem>
                    
                    {/* <NavItem>
                        <NavLink tag={RouteLink} to="/adminDetails">*SA* Patient Details</NavLink>
                    </NavItem> */}
                    
                    <NavItem>
                        <NavLink tag={RouteLink} to="/admin">*SA* Providers</NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
            </Container>
        </Navbar>
    )
}

export default Navigation