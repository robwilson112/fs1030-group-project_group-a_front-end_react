import React, { useState } from 'react';
import { Button, Col, Row,  Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter, Collapse, CardBody, Card } from 'reactstrap';
import { useHistory } from "react-router-dom";

const UpdatePatientForm = () => {
   
    // const [newPatient, setNewPatient] = useState({ 
    //     name: "", 
    //     email: "", 
    //     address: "", 
    //     city: "",  
    //     province: "", 
    //     postal "", 
    //     healthCard: "", 
    //     bloodType: "",
    //     DOB: "",
    //  });


    // const history = useHistory();

    // const handleChange = (event) => {
    //     setNewPatient((prevState) => ({
    //     ...prevState,
    //     [event.target.name]: event.target.value,
    //     }));
    // };

    // const handleSubmit = (event) => {
    //     fetch("/api/", {
    //     method: "post",
    //     headers: {
    //         Accept: "application/json",
    //         "Content-Type": "application/json",
    //     },

    //     //make sure to serialize your JSON body
    //     body: JSON.stringify(newPatient),
    //     }).then((response) => response.json());
    //     history.push("/");
    // };

   
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

      
    return(
        

        <Form>
            <div className="formSections">

            <Button color="primary" onClick={toggle} style={{ marginBottom: '1rem' }}>Contact Details</Button>
            <Collapse isOpen={isOpen}>
            <Card>
            <CardBody>
            <Row>
                <Col md={4}>
                <FormGroup>
                    <Label for="patientName">Phone</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                </FormGroup>
                </Col>
                </Row>
                <Row >
                    <Col md={6}>
                        <FormGroup>
                            <Label for="patientAdd">Address</Label>
                            <Input 
                            
                                type="text" 
                                name="patientAdd"
                                id="patientAdd" 
                                // value={newPatient.address} 
                                // onChange={handleChange} 
                            />
                        </FormGroup>
                    </Col>
                    <Col md={4}>
                    <FormGroup>
                        <Label for="patientCity">City</Label>
                        <Input 
                            type="text"
                            name="patientCity"
                            id="patientCity" 
                            // value={newPatient.city} 
                            // onChange={handleChange} 
                        />
                        
                    </FormGroup>
                    </Col>
                    </Row>
                    <Row>                  
                    <Col md={3}>
                    <FormGroup>
                        <Label for="patientProv">Province</Label>
                        <Input 
                            type="text" 
                            name="patientProv" 
                            id="patientProv" 
                            // value={newPatient.province} 
                            // onChange={handleChange} 
                        />
                    </FormGroup>
                    </Col>
                    <Col md={2}>              
                    <FormGroup>
                    <Label for="patientPC">Postal Code</Label>
                    <Input 
                        type="text" 
                        name="patientPC"
                        id="patientPC" 
                        // value={newPatient.postal} 
                        // onChange={handleChange} 
                     />
                </FormGroup>
                </Col>
                </Row>
                </CardBody>
                </Card>
                </Collapse>
                </div>

                <div className="formSections">

                    <Button color="primary" onClick={toggle} style={{ marginBottom: '1rem' }}>Visits</Button>
                    <Collapse isOpen={isOpen}>
                    <Card>
                    <CardBody>  
                    
                    {/* CREATE TABLE VISIT (
                    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                    date_time datetime,
                    reason VARCHAR(255),
                    diagnosis VARCHAR(255),
                    bill_number INT NOT NULL DEFAULT 0,
                    patient_id INT NOT NULL DEFAULT 0,
                    PRIMARY KEY (id)
                    ); */}
                    
                    <Row>
                    <Col md={4}>
                    <FormGroup>
                        <Label for="patientName">Date and Time</Label>
                        <Input 
                            type="text"
                            name="patientPhone" 
                            id="patientPhone" 
                            // value={newPatient.name} 
                            // onChange={handleChange} 
                        />
                    </FormGroup>
                    </Col>
                    </Row>

                    <FormGroup>
                        <Label for="patientName">Reason</Label>
                        <Input 
                            type="text"
                            name="patientPhone" 
                            id="patientPhone" 
                            // value={newPatient.name} 
                            // onChange={handleChange} 
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="patientName">Diagnosis</Label>
                        <Input 
                            type="text"
                            name="patientPhone" 
                            id="patientPhone" 
                            // value={newPatient.name} 
                            // onChange={handleChange} 
                        />
                    </FormGroup>
                    <Col md={2}>
                    <FormGroup >
                        <Label for="patientName">Bill No.</Label>
                        <Input 
                            type="text"
                            name="patientPhone" 
                            id="patientPhone" 
                            // value={newPatient.name} 
                            // onChange={handleChange} 
                        />
                    </FormGroup>
                    </Col>
                    </CardBody>
                    </Card>
                    </Collapse>
                </div>



                <p className="formSections">Lab Tests</p>
                    {/* CREATE TABLE LAB_TESTS (
                    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL DEFAULT '',
                    results VARCHAR(255) NOT NULL DEFAULT '',
                    Radiology_Image varbinary(9999),
                    PRIMARY KEY (id)
                    ); */}

                <FormGroup >
                    <Label for="patientName">Test Name</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>

                <FormGroup >
                    <Label for="patientName">Results</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>
                    

                <p className="formSections">Medical History</p>

                {/* CREATE TABLE MEDICAL_HISTORY (
                id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                physical_exams VARCHAR(999) NOT NULL DEFAULT '',
                medicines_taken VARCHAR(255) NOT NULL DEFAULT '',
                allergies VARCHAR(255) NOT NULL DEFAULT '',
                immunization_status VARCHAR(255) NOT NULL DEFAULT '',
                surgeries VARCHAR(9999) NOT NULL DEFAULT '',
                patient_id INT NOT NULL DEFAULT 0,
                PRIMARY KEY (id)
                ); */}

                <FormGroup >
                    <Label for="patientName">Physical Exams</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>

                <FormGroup >
                    <Label for="patientName">Medications</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>    

                <FormGroup >
                    <Label for="patientName">Immunization Status</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>    


                <FormGroup >
                    <Label for="patientName">Surgeries</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>        

                <p className="formSections">Billing</p>

                    {/* CREATE TABLE BILL (
                    bill_number INT UNSIGNED NOT NULL AUTO_INCREMENT,
                    date_time datetime,
                    status CHAR(15),
                    bill_details VARCHAR(255),
                    amount decimal(7,2),
                    patient_id INT NOT NULL DEFAULT 0,
                    receipt_id INT NOT NULL DEFAULT 0,
                    PRIMARY KEY (bill_number)
                    ); */}

                    <FormGroup >
                    <Label for="patientName">Bill No.</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>

                    <FormGroup >
                    <Label for="patientName">Date and Time</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>

                    <FormGroup >
                    <Label for="patientName">Status</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>

                    <FormGroup >
                    <Label for="patientName"> Bill Details</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>

                    <FormGroup >
                    <Label for="patientName">Ammount</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>


                <p className="formSections">Payments</p>

                    {/* CREATE TABLE PAYMENT (
                    receipt_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                    amount decimal(7,2),
                    method CHAR(15),
                    date_time datetime,
                    patient_id INT NOT NULL DEFAULT 0,
                    bill_number INT NOT NULL DEFAULT 0,
                    PRIMARY KEY (receipt_id)
                    );        */}


                <FormGroup >
                    <Label for="patientName">Reciept No.</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>
                    <FormGroup >
                    <Label for="patientName">Amount Paid</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>
                    <FormGroup >
                    <Label for="patientName">Method</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>
                    <FormGroup >
                    <Label for="patientName">Date and Time</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>
                    <FormGroup >
                    <Label for="patientName">Bill No.</Label>
                    <Input 
                        type="text"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>


                <p className="formSections">Notes</p>

                    <FormGroup >
                    <Label for="patientName"></Label>
                    <Input 
                        type="textarea"
                        name="patientPhone" 
                        id="patientPhone" 
                        // value={newPatient.name} 
                        // onChange={handleChange} 
                    />
                    </FormGroup>

                <input type="submit" value="Submit" />
                <input type="submit" value="Cancel" />
               </Form>

  )
}

export default UpdatePatientForm

            