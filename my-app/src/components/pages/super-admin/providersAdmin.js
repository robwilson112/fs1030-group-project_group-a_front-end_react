import React from 'react'
import MockData from '../MOCK_DATA.json'
import {useState} from 'react'
import {Link} from 'react-router-dom'
import { Button, Table } from 'reactstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import ProvModalForm from '../../shared/super-admin/newProviderModal';



const Admin = () => {

    return(
        <div className="containerMain">
                      
            {/*search container */}
            <div className="searchContainer">

                {/*search input */}
                <div className="searchInput">

                    <h1 className="header">Care Providers</h1> 

                     <div className="newPatient">

                        <ProvModalForm />
                     </div>

                  <div className="searchInputProv"> 

                  <table className="tableSearch">
                        <thead className="tableHeadSA">
                            <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td scope="row">1</td>
                            <td>Mark</td>
                            <td>@mdo</td>
                            <td>Super Admin</td>
                            <td><button className="btn btn-primary">Edit</button></td>
                            <td><button className="btn btn-primary">Delete</button></td>
                            </tr>
                            <tr>
                            <td scope="row">2</td>
                            <td>Jacob</td>
                            <td>@fat</td>
                            <td>Super Admin</td>
                            <td><button className="btn btn-primary">Edit</button></td>
                            <td><button className="btn btn-primary">Delete</button></td>
                            </tr>
                            <tr>
                            <td scope="row">3</td>
                            <td>Larry</td>
                            <td>@twitter</td>
                            <td>Super Admin</td>
                            <td><button className="btn btn-primary">Edit</button></td>
                            <td><button className="btn btn-primary">Delete</button></td>
                            </tr>
                        </tbody>
                        </table>  

                    </div> 
                    
                    
                </div>
            </div>
        </div>
     );

} 
   


export default Admin;
