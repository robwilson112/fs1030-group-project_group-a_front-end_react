import React from 'react'
import { Container, Col, Row, Button, Form, FormGroup, Label, Input, Card, CardBody, CardText } from 'reactstrap'
import splash from '../../assets/images/splash.jpg'
import { useHistory, useLocation } from 'react-router-dom'


const Home = () => {

    // let history = useHistory();
    //   let location = useLocation();
    //   const [username, setUsername] = useState("")
    //   const [password, setPassword] = useState("")
    //   const [auth, setAuth] = useState(true)

    //   const loginSubmit = async event => {
          
    //       event.preventDefault()
    //       const response = await fetch('http://localhost:4000/auth', {
    //           method: 'POST',
    //           headers: {
    //               'Accept': 'application/json',
    //               'Content-Type': 'application/json'
    //             },
    //           body: JSON.stringify({username, password})
    //       })
    //       const payload = await response.json()
    //       if (response.status >= 400) {
    //           setAuth(false)
    //       } else {
    //           sessionStorage.setItem('token', payload.token)

    //           let { from } = location.state || { from: { pathname: "/" } };
    //           history.replace(from);
    //       }
    //   }



    return (
        <main className="containerMain">
            
        {/*homepage background image*/}
        <img className="splashImg" src={splash} alt="image of a laptop a notebad and a HTML and CSS for beginners book arranged on a desk"></img>
        <div className="containerLogin">
        <p className="formCardHeader">
            <h4 className="loginHeader">Login</h4>
        </p>
        <div> test
          {/* login error message here */}

            {/* {!auth && 
                <Card className="text-white bg-primary my-5 py-4 text-center" className="formCardErr">
                <CardBody>
                    <CardText className="text-white m-0" className="formCardTextErr"> &#10071; Invalid credentials, please try again &#10071;</CardText>
                </CardBody>
            </Card>
            } */}
        </div>

        {/*login form */}
        <Form 
          className="my-5"
          className="contactForm"
        //  onSubmit={loginSubmit}
         > 
          <Row form>
            <Col md={11}>
              <FormGroup>
                <Label for="usernameEntry"></Label>
                <Input type="text" className="field" name="username" id="usernameEntry" placeholder="Username" bsSize="lg" />
              </FormGroup>
            </Col>
            </Row>
            <Row>
            <Col md={11}>
              <FormGroup>
                <Label for="passwordEntry"></Label>
                <Input type="password" className="field" name="password" id="passwordEntry" placeholder="Valid password" bsSize="lg" />
              </FormGroup>
            </Col>
          </Row>
          <Button className="loginButton" color="warning"><strong>Sign in</strong></Button>
          
        </Form>
        </div>
        
        </main>



  );
}


export default Home