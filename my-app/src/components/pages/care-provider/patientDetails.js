import React, {useState} from 'react'
import { TabContent, Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import UpdateModalForm from '../../shared/updatePatientModal';


const Details = (props) => {

            
  
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }

  return (
      
    <div className="containerMain">

      {/*details container */}
        <div className="detailDisplay">

       {/*details patient card display */}   
       <h1 className="detailsHeadTitle">
           Patient Details
       </h1>    
       <div className="detailsHeader">
        <p>Patient ID : Fake ID</p>
        <p>Patient Name : Fake Name</p>
        <p>Patient DOB : 1900/01/01</p>
        <p>Gender : F</p>
        <p>Blood Type: A</p>
        <p>Health Card Number : xxxx xxxxx</p>
      </div>

        <div className="updateButton">
          <UpdateModalForm />
        </div>

      {/*tabs container */}
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            <strong>Contact Details</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
           <strong>Visits</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >
           <strong>Lab Tests</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '4' })}
            onClick={() => { toggle('4'); }}
          >
           <strong>Medical History</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '5' })}
            onClick={() => { toggle('5'); }}
          >
           <strong>Billing and Payments</strong>
          </NavLink>
        </NavItem>
        <NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '6' })}
            onClick={() => { toggle('6'); }}
          >
           <strong>Notes</strong>
          </NavLink>
        </NavItem>     

              
          
        </NavItem>

      </Nav>

      {/*tabs content */}
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1"> 
          <Row>
                         
              <h4 className="detailsTitle">Contact Details</h4>
              
              <div className="detailsBox">
                <p>Email: </p>
                <p>Phone: </p>
                <p>Address: </p>
                <p>Postal Code: </p>
                <p>Province: </p>
              </div>             
            
          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
            <h4 className="detailsTitle">Visits</h4>
            <div className="detailsBox">

            <Table>
                <thead>
                  <tr>
                    <th>Visit No.</th>
                    <th>Date and Time</th>
                    <th>Reason</th>
                    <th>Diagnosis</th>
                    <th>Bill No.</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>March 1</td>
                    <td>Sick</td>
                    <td>Too much coding</td>
                    <td>555</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>June 2</td>
                    <td>Sick</td>
                    <td>Not enough coding</td>
                    <td>556</td>
                  </tr>
                  
                </tbody>
            </Table>
             
            </div>
            
          </Row>
        </TabPane>
        <TabPane tabId="3">
          <Row>
            
            <h4 className="detailsTitle">Tests</h4>
            <div className="detailsBox">
              
              <Table>
                <thead>
                  <tr>
                    <th>Test No.</th>
                    <th>Date and Time</th>
                    <th>Test Name</th>
                    <th>Results</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>April 5</td>
                    <td>Sick</td>
                    <td>Too much coding</td>
                  </tr>
                </tbody>
            </Table>


            </div>
          </Row>
        </TabPane>
        <TabPane tabId="4">
          <Row>
          
            <h4 className="detailsTitle">History</h4>
            <div className="detailsBox">
              
              <Table>
                <thead>
                  <tr>
                    <th>Exam No.</th>
                    <th>Physical Exam</th>
                    <th>Medication</th>
                    <th>Immunization Status</th>
                    <th>Surgery</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Null</td>
                    <td>Null</td>
                    <td>Null</td>
                    <td>Null</td>
                  </tr>
                </tbody>
            </Table>


            </div>
          </Row>
        </TabPane>
        <TabPane tabId="5">
          <Row>
          <h4 className="detailsTitle">Bills</h4>
           <div className="detailsBox">
              
            <Table>
                <thead>
                  <tr>
                    <th>Bill No.</th>
                    <th>Date and Time</th>
                    <th>Status</th>
                    <th>Bill Details</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Null</td>
                    <td>Null</td>
                    <td>Null</td>
                    <td>Null</td>
                  </tr>
                </tbody>
            </Table>

            </div>
          </Row>
          <Row>
          <h4 className="detailsTitle">Payments</h4>
           <div className="detailsBox">
              
            <Table>
                <thead>
                  <tr>
                    <th>Reciept No.</th>
                    <th>Amount Paid</th>
                    <th>Method</th>
                    <th>Date and Time</th>
                    <th>Bill No.</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Null</td>
                    <td>Null</td>
                    <td>Null</td>
                    <td>Null</td>
                  </tr>
                </tbody>
            </Table>

            </div>
          </Row>
        </TabPane>
        <TabPane tabId="6">
          <Row>
          <h4 className="detailsTitle">Notes</h4>
           <div className="detailsBox">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              
            </div>
          </Row>
        </TabPane>
      </TabContent>
      </div>
    </div>
    
  );
}




export default Details