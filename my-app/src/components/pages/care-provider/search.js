import React from 'react'
import MockData from '../MOCK_DATA.json'
import {useState} from 'react'
import {Link} from 'react-router-dom'
import { Button } from 'reactstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import ModalForm from '../../shared/newPatientModal';


const Search = () => {

    {/*use state search functionality */}
    const [searchTerm, setsearchTerm] = useState("")

    
   
   document.querySelectorAll('#searchTable button.btn.btn-primary').forEach(function(element){
        element.addEventListener('click', function(e){
            let row = this.closest('tr');
            let rowID = row.cells[0].textContent;
            console.log(rowID)
        })
    })


    return(
        <div className="containerMain">

                       
           
           {/*search container */}
            <div className="searchContainer">

            {/*search input */}
                <div className="searchInput">

                <h1 className="header">Patients</h1>

                <div className="newPatient">
                    <ModalForm />
                    

                    </div>


            <input 
            type="text" 
            placeholder="Patient Search" 
            className="form-control" 
            className="searchBar"
            onChange = {(e)=>{
                setsearchTerm(e.target.value);
            }}
            ></input>
            </div>

            {/* patient data table */}
            <table className="tableSearch" id="searchTable">
                <thead className="headerCP">
                    <tr>
                        <th className="head1">ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th className="head2">Select</th>
                    </tr>
                </thead>
            <tbody>

             
                {/* load table data */}
                    {MockData.filter(value=>{
                        if(searchTerm === ''){
                            return value;
                        } else if(
                            value.first_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
                            value.last_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
                            value.email.toLowerCase().includes(searchTerm.toLowerCase())
                        ){
                            return value
                        }
                        }
                    ).map((m)=> (
                        <tr key={m.id}>
                                <td>{m.id}</td>
                                <td>{m.first_name}</td>
                                <td>{m.last_name}</td>
                                <td>{m.email}</td>
                                <td><button className="btn btn-primary">Select</button></td>
                                
                        </tr>
                    ))}
                </tbody>
            </table>
           
            </div>
        </div>
     );

}

export default Search;
